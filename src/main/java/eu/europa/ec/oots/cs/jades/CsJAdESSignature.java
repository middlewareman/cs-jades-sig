package eu.europa.ec.oots.cs.jades;

import eu.europa.esig.dss.enumerations.*;
import eu.europa.esig.dss.jades.HTTPHeaderDigest;
import eu.europa.esig.dss.jades.JAdESSignatureParameters;
import eu.europa.esig.dss.jades.signature.JAdESService;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.model.ToBeSigned;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.Pkcs12SignatureToken;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class CsJAdESSignature {

    private static final Logger log = LoggerFactory.getLogger(CsJAdESSignature.class);
    private SignatureValue signatureValue;
    private DSSPrivateKeyEntry privateKey;
    private DSSDocument signedDocument;
    private JAdESSignatureParameters parameters;
    private Pkcs12SignatureToken token;
    private JAdESService jAdESService;
    private static final String DOCUMENT_NAME = "oots-response-sig";

    private final CertificateProperties certificateProperties;
    private final CommonCertificateVerifier commonCertificateVerifier;

    public CsJAdESSignature() {
        this(getCertificateProperties(), getCommonCertificateVerifier());
    }

    public CsJAdESSignature(CertificateProperties certificateProperties, CommonCertificateVerifier commonCertificateVerifier) {
        this.certificateProperties = certificateProperties;
        this.commonCertificateVerifier = commonCertificateVerifier;
        init();
    }

    /**
     * Initialization of the certificate properties, service and private key
     */
    public void init() {
        if (certificateProperties == null || certificateProperties.getFilename() == null || certificateProperties.getPassword() == null) {
            return;
        }
        // Load certificate
        InputStream inputStream = loadCertificate(certificateProperties.getFilename());

        String password = certificateProperties.getPassword();

        jAdESService = new JAdESService(commonCertificateVerifier);
        token = new Pkcs12SignatureToken(inputStream, new KeyStore.PasswordProtection(password.toCharArray()));
        List<DSSPrivateKeyEntry> keys = token.getKeys();
        for (DSSPrivateKeyEntry entry : keys) {
            privateKey = entry;
            log.debug("{}", entry.getCertificate().getCertificate().toString());
        }

        // JAdES signature params
        parameters = new JAdESSignatureParameters();
        assert privateKey != null;
        parameters.setSigningCertificate(privateKey.getCertificate());
        parameters.setCertificateChain(privateKey.getCertificateChain());
        // Set Detached packaging - STEP 1
        parameters.setSignaturePackaging(SignaturePackaging.DETACHED);
        // Set Mechanism HttpHeaders for 'sigD' header - STEP 2 and STEP 3
        parameters.setSigDMechanism(SigDMechanism.HTTP_HEADERS);
        parameters.setBase64UrlEncodedPayload(false);
        parameters.setJwsSerializationType(JWSSerializationType.COMPACT_SERIALIZATION);
        parameters.setSignatureLevel(SignatureLevel.JAdES_BASELINE_B);
        // STEP 5
        parameters.setEncryptionAlgorithm(EncryptionAlgorithm.ECDSA);
        parameters.setDigestAlgorithm(DigestAlgorithm.SHA256);
    }

    /**
     * Signs a json string with ETSI ESI Jades compliant signature rules on :
     * <a href="https://ec.europa.eu/digital-building-blocks/wikis/display/TDD/3.1.4+Query+Interface+Specification+of+the+DSD#id-3.1.4QueryInterfaceSpecificationoftheDSD-4.6.ResponseSignature">...</a>
     *
     * @param json with payload
     * @return CsJades object with digest and signed document
     **/
    public CsJades buildSignedDocument(String json) throws IOException {
        CsJades csJades = new CsJades();
        DSSDocument toSignDocument = new InMemoryDocument(json.getBytes());
        toSignDocument.setName(DOCUMENT_NAME);
        // STEP 4
        HTTPHeaderDigest digest = new HTTPHeaderDigest(toSignDocument, DigestAlgorithm.SHA256);
        csJades.setDigest(digest.getValue());
        List<DSSDocument> documentsToSign = new ArrayList<>();
        documentsToSign.add(digest);

        ToBeSigned toBeSigned = jAdESService.getDataToSign(documentsToSign, parameters);
        signatureValue = token.sign(toBeSigned, DigestAlgorithm.SHA256, privateKey);
        signedDocument = jAdESService.signDocument(digest, parameters, signatureValue);

        csJades.setSignedDocument(new String(signedDocument.openStream().readAllBytes()));

        return csJades;
    }

    public static CertificateProperties getCertificateProperties() {
        try (InputStream input =
                     CsJAdESSignature.class.getClassLoader().getResourceAsStream("config.properties")) {
            Properties prop = new Properties();

            if (input == null) {
                log.error("Unable to find config.properties");
                return null;
            }

            prop.load(input);
            CertificateProperties properties = new CertificateProperties();
            properties.setFilename(prop.getProperty("certificate.filename"));
            properties.setPassword(prop.getProperty("certificate.password"));
            return properties;
        } catch (IOException ex) {
            log.error("An error occurred while loading config.");
        }
        return null;
    }

    private InputStream loadCertificate(String filename) {
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(filename);
        } catch (IOException e) {
            log.error("Could not load from file, trying from classloader: {}", e.getMessage());
            ClassLoader classLoader = getClass().getClassLoader();
            inputStream = classLoader.getResourceAsStream(filename);
        }
        return inputStream;
    }

    private static CommonCertificateVerifier getCommonCertificateVerifier() {
        return new CommonCertificateVerifier();
    }

    public SignatureValue getSignatureValue() {
        return signatureValue;
    }

    public DSSDocument getSignedDocument() {
        return signedDocument;
    }

    public DSSPrivateKeyEntry getPrivateKey() {
        return privateKey;
    }
}
