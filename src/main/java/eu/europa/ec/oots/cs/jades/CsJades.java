package eu.europa.ec.oots.cs.jades;

public class CsJades {

    private String digest;
    private String signedDocument;

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getSignedDocument() {
        return signedDocument;
    }

    public void setSignedDocument(String signedDocument) {
        this.signedDocument = signedDocument;
    }
}
