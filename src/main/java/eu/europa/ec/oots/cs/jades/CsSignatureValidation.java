package eu.europa.ec.oots.cs.jades;

public class CsSignatureValidation {
    private String validationInfo;
    private boolean isValid;

    public CsSignatureValidation(boolean isValid, String validationInfo) {
        this.isValid = isValid;
        this.validationInfo = validationInfo;
    }

    public String getValidationInfo() {
        return validationInfo;
    }

    public void setValidationInfo(String validationInfo) {
        this.validationInfo = validationInfo;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}