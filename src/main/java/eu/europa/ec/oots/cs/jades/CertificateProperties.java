package eu.europa.ec.oots.cs.jades;

public class CertificateProperties {

    private String filename;
    private String password;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
