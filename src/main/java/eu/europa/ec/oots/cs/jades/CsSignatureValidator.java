package eu.europa.ec.oots.cs.jades;

import eu.europa.esig.dss.detailedreport.jaxb.XmlSemantic;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.jades.HTTPHeader;
import eu.europa.esig.dss.jades.HTTPHeaderDigest;
import eu.europa.esig.dss.jades.validation.JWSCompactDocumentValidator;
import eu.europa.esig.dss.jaxb.object.Message;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.spi.x509.CommonTrustedCertificateSource;
import eu.europa.esig.dss.spi.x509.KeyStoreCertificateSource;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.validation.executor.ValidationLevel;
import eu.europa.esig.dss.validation.reports.Reports;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public class CsSignatureValidator{
    public static final String DEFAULT_CS_TRUSTSTORE = "cs-truststore.jks";
    public static final String DEFAULT_CS_TRUSTSTORE_PASSWORD = "changeit";
    public static final String CS_DSS_POLICY_XML = "cs-dss-policy.xml";
    private final String trustStoreFileName;
    private final String trustStorePassword;

    public CsSignatureValidator() {
        this.trustStoreFileName = DEFAULT_CS_TRUSTSTORE;
        this.trustStorePassword = DEFAULT_CS_TRUSTSTORE_PASSWORD;
    }

    public CsSignatureValidator(String trustStoreFileName, String trustStorePassword) {
        this.trustStoreFileName = trustStoreFileName;
        this.trustStorePassword = trustStorePassword;
    }

    public CsSignatureValidation validate(String xmlQueryResponse, String signature, String digest) {
        DSSDocument document = new InMemoryDocument(signature.getBytes());
        HTTPHeader httpHeader = new HTTPHeader("digest", digest);
        JWSCompactDocumentValidator documentValidator = new JWSCompactDocumentValidator(document);
        CommonCertificateVerifier commonCertificateVerifier = new CommonCertificateVerifier();
        documentValidator.setCertificateVerifier(commonCertificateVerifier);

        StringBuilder sb = new StringBuilder();

        documentValidator.setDetachedContents(Arrays.asList(document, httpHeader));
        documentValidator.setValidationLevel(ValidationLevel.BASIC_SIGNATURES);
        documentValidator.setEnableEtsiValidationReport(true);
        documentValidator.setIncludeSemantics(true);

        InputStream trustStoreStream = this.getClass().getClassLoader().getResourceAsStream(trustStoreFileName);
        KeyStoreCertificateSource trustStore =
                new KeyStoreCertificateSource(trustStoreStream,"JKS", trustStorePassword.toCharArray());

        CertificateVerifier cerVer = new CommonCertificateVerifier();
        CommonTrustedCertificateSource trustedCertificateSource = new CommonTrustedCertificateSource();
        trustedCertificateSource.importAsTrusted(trustStore);
        cerVer.setTrustedCertSources(trustedCertificateSource);
        documentValidator.setCertificateVerifier(cerVer);

        InputStream csDssPolicyStream = this.getClass().getClassLoader().getResourceAsStream(CS_DSS_POLICY_XML);
        Reports reports = documentValidator.validateDocument(csDssPolicyStream);
        List<XmlSemantic> semantics = reports.getDetailedReport().getJAXBModel().getSemantic();

        for ( Message msg:  reports.getSimpleReport().getAdESValidationErrors(reports.getSimpleReport().getFirstSignatureId())) {
            sb.append(msg.getKey()).append(" ").append(msg.getValue()).append("\n");
        }

        for ( Message msg:  reports.getSimpleReport().getAdESValidationInfo(reports.getSimpleReport().getFirstSignatureId())) {
            sb.append(msg.getKey()).append(" ").append(msg.getValue()).append("\n");
        }

        for(XmlSemantic semantic : semantics){
            sb.append(semantic.getKey()).append(" ").append(semantic.getValue()).append("\n");
        }

        boolean isDigestValid = isDigestValid(xmlQueryResponse, digest, documentValidator, sb);

        //Expecting one valid signature in case it's a valid one
        boolean isValid =  reports.getSimpleReport().getValidSignaturesCount() == 1 && isDigestValid;

        return new CsSignatureValidation(isValid, sb.toString());
    }

    private boolean isDigestValid(String xmlQueryResponse, String digest, JWSCompactDocumentValidator documentValidator, StringBuilder sb) {
        String queryResponseDigest = "";

        DigestAlgorithm digestAlgorithm = documentValidator.getSignatures().get(0) != null ? documentValidator.getSignatures().get(0).getDigestAlgorithm() : null;
        if(DigestAlgorithm.SHA256.equals(digestAlgorithm)) {
            HTTPHeaderDigest httpHeaderDigest = new HTTPHeaderDigest(new InMemoryDocument(xmlQueryResponse.getBytes(StandardCharsets.UTF_8)), DigestAlgorithm.SHA256);
            queryResponseDigest = httpHeaderDigest.getValue();
        }
        else if(DigestAlgorithm.SHA512.equals(digestAlgorithm)) {
            HTTPHeaderDigest httpHeaderDigest = new HTTPHeaderDigest(new InMemoryDocument(xmlQueryResponse.getBytes(StandardCharsets.UTF_8)), DigestAlgorithm.SHA512);
            queryResponseDigest = httpHeaderDigest.getValue();
        }
        else {
            sb.append("DIGEST IS INVALID: Signature Digest Algorithm ");
            sb.append(digestAlgorithm);
            sb.append(" not supported. Supported ones are: SHA-256 and SHA-512\n");
        }

        boolean isDigestValid = queryResponseDigest.equals(digest);
        if(isDigestValid) {
            sb.append("DIGEST IS VALID: XML QueryResponse produced digest matches with received one\n");
        }
        else {
            sb.append("DIGEST IS INVALID: XML QueryResponse produced digest doesn't match with received one\n");
        }

        return isDigestValid;
    }
}