package eu.europa.ec.oots.cs.validator;


import eu.europa.ec.oots.cs.jades.CsSignatureValidation;
import eu.europa.ec.oots.cs.jades.CsSignatureValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CsSignatureTest {

    @Test
    void testValidCsSignatureMsIt() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator();

        String xmlQueryResponse = "<?xml version='1.0' encoding='UTF-8'?><query:QueryResponse xmlns=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:sdg=\"http://data.europa.eu/p4s\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\">\n" +
                "  <rs:Exception xsi:type=\"rs:InvalidRequestExceptionType\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\" message=\"The query parameters do not follow the query specification\" code=\"DSD:ERR:0003\" detail=\"Mandatory CountryCode parameter missing.\"/>\n" +
                "</query:QueryResponse>\n";
        String signature = "eyJhbGciOiJFZERTQSIsImtpZCI6Ik1Ha3dVYVJQTUUweEN6QUpCZ05WQkFZVEFrbFVNUTR3REFZRFZRUUlEQVZKZEdGc2VURU5NQXNHQTFVRUNnd0VRV2RKUkRFTk1Bc0dBMVVFQ3d3RVFXZEpSREVRTUE0R0ExVUVBd3dIWldReU5UVXhPUUlVY0RFcWc1SDd6aW1DZ0dFZHF0ZUl4YU9GQy9ZPSIsIng1dCNTMjU2Ijoic1FNVGlza1dMV2JTRW9pUEZIM1RtN0FiOERHYVBZMkE5ZjcydXEtS0YxYyIsIng1YyI6WyJNSUlCVlRDQ0FRY0NGSEF4S29PUis4NHBnb0JoSGFyWGlNV2poUXYyTUFVR0F5dGxjREJOTVFzd0NRWURWUVFHRXdKSlZERU9NQXdHQTFVRUNBd0ZTWFJoYkhreERUQUxCZ05WQkFvTUJFRm5TVVF4RFRBTEJnTlZCQXNNQkVGblNVUXhFREFPQmdOVkJBTU1CMlZrTWpVMU1Ua3dIaGNOTWpNeE1ERTJNVFF3TkRRM1doY05NalV3T1RFMU1UUXdORFEzV2pCTk1Rc3dDUVlEVlFRR0V3SkpWREVPTUF3R0ExVUVDQXdGU1hSaGJIa3hEVEFMQmdOVkJBb01CRUZuU1VReERUQUxCZ05WQkFzTUJFRm5TVVF4RURBT0JnTlZCQU1NQjJWa01qVTFNVGt3S2pBRkJnTXJaWEFESVFCK2VlQmptUFNrLzlrQXVNMmxXaTA2OWFUQ3BtRlp6L0hXN3ljY20yUUZJekFGQmdNclpYQURRUURkcldTdjJUWHNmL3FuTTdOOWNGRHFkWm50ZjhjaWk2am4vL3dNeEcweHMvekMybUdkVDc0dzV3VDdqSk5EcGFrVm92cmRuYkUybVVTWUI1YWppRFVIIl0sInR5cCI6Impvc2UiLCJiNjQiOmZhbHNlLCJzaWdUIjoiMjAyMy0xMi0yMFQxNToyNDoxMVoiLCJzaWdEIjp7Im1JZCI6Imh0dHA6Ly91cmkuZXRzaS5vcmcvMTkxODIvSHR0cEhlYWRlcnMiLCJwYXJzIjpbImRpZ2VzdCJdfSwiY3JpdCI6WyJiNjQiLCJzaWdUIiwic2lnRCJdfQ..KbONP_Hj1fSwL1_h-UBHOj8E-ZkMbNI3C0Chup5htO-cvRTC9pKgGK40g2JbKAF68iCJe4xyFC0wnHZ_h1yLAA";
        String digest = "SHA-512=RHkXWuDWRZ25GD/qC6opnKdJdqdY07vo4NdciXg2/bm0HialSWXnlesDgRKhMVu+24d6l6h7/uHf97pZRHjvJA==";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertTrue(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testTamperedSignatureCsSignatureMsIt() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator();
        String xmlQueryResponse = "<?xml version='1.0' encoding='UTF-8'?><query:QueryResponse xmlns=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:sdg=\"http://data.europa.eu/p4s\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\">\n" +
                "  <rs:Exception xsi:type=\"rs:InvalidRequestExceptionType\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\" message=\"The query parameters do not follow the query specification\" code=\"DSD:ERR:0003\" detail=\"Mandatory CountryCode parameter missing.\"/>\n" +
                "</query:QueryResponse>\n";
        String signature = "eyJhbGciOiJFZERTQSIsImtpZCI6Ik1Ha3dVYVJQTUUweEN6QUpCZ05WQkFZVEFrbFVNUTR3REFZRFZRUUlEQVZKZEdGc2VURU5NQXNHQTFVRUNnd0VRV2RKUkRFTk1Bc0dBMVVFQ3d3RVFXZEpSREVRTUE0R0ExVUVBd3dIWldReU5UVXhPUUlVY0RFcWc1SDd6aW1DZ0dFZHF0ZUl4YU9GQy9ZPSIsIng1dCNTMjU2Ijoic1FNVGlza1dMV2JTRW9pUEZIM1RtN0FiOERHYVBZMkE5ZjcydXEtS0YxYyIsIng1YyI6WyJNSUlCVlRDQ0FRY0NGSEF4S29PUis4NHBnb0JoSGFyWGlNV2poUXYyTUFVR0F5dGxjREJOTVFzd0NRWURWUVFHRXdKSlZERU9NQXdHQTFVRUNBd0ZTWFJoYkhreERUQUxCZ05WQkFvTUJFRm5TVVF4RFRBTEJnTlZCQXNNQkVGblNVUXhFREFPQmdOVkJBTU1CMlZrTWpVMU1Ua3dIaGNOTWpNeE1ERTJNVFF3TkRRM1doY05NalV3T1RFMU1UUXdORFEzV2pCTk1Rc3dDUVlEVlFRR0V3SkpWREVPTUF3R0ExVUVDQXdGU1hSaGJIa3hEVEFMQmdOVkJBb01CRUZuU1VReERUQUxCZ05WQkFzTUJFRm5TVVF4RURBT0JnTlZCQU1NQjJWa01qVTFNVGt3S2pBRkJnTXJaWEFESVFCK2VlQmptUFNrLzlrQXVNMmxXaTA2OWFUQ3BtRlp6L0hXN3ljY20yUUZJekFGQmdNclpYQURRUURkcldTdjJUWHNmL3FuTTdOOWNGRHFkWm50ZjhjaWk2am4vL3dNeEcweHMvekMybUdkVDc0dzV3VDdqSk5EcGFrVm92cmRuYkUybVVTWUI1YWppRFVIIl0sInR5cCI6Impvc2UiLCJiNjQiOmZhbHNlLCJzaWdUIjoiMjAyMy0xMi0yMFQxNToyNDoxMVoiLCJzaWdEIjp7Im1JZCI6Imh0dHA6Ly91cmkuZXRzaS5vcmcvMTkxODIvSHR0cEhlYWRlcnMiLCJwYXJzIjpbImRpZ2VzdCJdfSwiY3JpdCI6WyJiNjQiLCJzaWdUIiwic2lnRCJdfQ..KbONP_Hj1fSwL1_h-UBHOj8E-ZkMbNI3C0Chup5htO-cvRTC9pKgGK40g2JbKAF68iCJe4xyFC0wnHTAMPERED";
        String digest = "SHA-512=RHkXWuDWRZ25GD/qC6opnKdJdqdY07vo4NdciXg2/bm0HialSWXnlesDgRKhMVu+24d6l6h7/uHf97pZRHjvJA==";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertFalse(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testTamperedDigestCsSignatureMsIt() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator();

        String xmlQueryResponse = "<?xml version='1.0' encoding='UTF-8'?><query:QueryResponse xmlns=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:sdg=\"http://data.europa.eu/p4s\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\">\n" +
                "  <rs:Exception xsi:type=\"rs:InvalidRequestExceptionType\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\" message=\"The query parameters do not follow the query specification\" code=\"DSD:ERR:0003\" detail=\"Mandatory CountryCode parameter missing.\"/>\n" +
                "</query:QueryResponse>\n";
        String signature = "eyJhbGciOiJFZERTQSIsImtpZCI6Ik1Ha3dVYVJQTUUweEN6QUpCZ05WQkFZVEFrbFVNUTR3REFZRFZRUUlEQVZKZEdGc2VURU5NQXNHQTFVRUNnd0VRV2RKUkRFTk1Bc0dBMVVFQ3d3RVFXZEpSREVRTUE0R0ExVUVBd3dIWldReU5UVXhPUUlVY0RFcWc1SDd6aW1DZ0dFZHF0ZUl4YU9GQy9ZPSIsIng1dCNTMjU2Ijoic1FNVGlza1dMV2JTRW9pUEZIM1RtN0FiOERHYVBZMkE5ZjcydXEtS0YxYyIsIng1YyI6WyJNSUlCVlRDQ0FRY0NGSEF4S29PUis4NHBnb0JoSGFyWGlNV2poUXYyTUFVR0F5dGxjREJOTVFzd0NRWURWUVFHRXdKSlZERU9NQXdHQTFVRUNBd0ZTWFJoYkhreERUQUxCZ05WQkFvTUJFRm5TVVF4RFRBTEJnTlZCQXNNQkVGblNVUXhFREFPQmdOVkJBTU1CMlZrTWpVMU1Ua3dIaGNOTWpNeE1ERTJNVFF3TkRRM1doY05NalV3T1RFMU1UUXdORFEzV2pCTk1Rc3dDUVlEVlFRR0V3SkpWREVPTUF3R0ExVUVDQXdGU1hSaGJIa3hEVEFMQmdOVkJBb01CRUZuU1VReERUQUxCZ05WQkFzTUJFRm5TVVF4RURBT0JnTlZCQU1NQjJWa01qVTFNVGt3S2pBRkJnTXJaWEFESVFCK2VlQmptUFNrLzlrQXVNMmxXaTA2OWFUQ3BtRlp6L0hXN3ljY20yUUZJekFGQmdNclpYQURRUURkcldTdjJUWHNmL3FuTTdOOWNGRHFkWm50ZjhjaWk2am4vL3dNeEcweHMvekMybUdkVDc0dzV3VDdqSk5EcGFrVm92cmRuYkUybVVTWUI1YWppRFVIIl0sInR5cCI6Impvc2UiLCJiNjQiOmZhbHNlLCJzaWdUIjoiMjAyMy0xMi0yMFQxNToyNDoxMVoiLCJzaWdEIjp7Im1JZCI6Imh0dHA6Ly91cmkuZXRzaS5vcmcvMTkxODIvSHR0cEhlYWRlcnMiLCJwYXJzIjpbImRpZ2VzdCJdfSwiY3JpdCI6WyJiNjQiLCJzaWdUIiwic2lnRCJdfQ..KbONP_Hj1fSwL1_h-UBHOj8E-ZkMbNI3C0Chup5htO-cvRTC9pKgGK40g2JbKAF68iCJe4xyFC0wnHZ_h1yLAA";
        String digest = "SHA-512=tamperedRZ25GD/qC6opnKdJdqdY07vo4NdciXg2/bm0HialSWXnlesDgRKhMVu+24d6l6h7/uHf97pZRHjvJA==";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertFalse(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testDigestNotMatchingDigestCsSignatureMsIt() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator();

        String xmlQueryResponse = "<?xml version='1.0' encoding='UTF-8'?><query:QueryResponse xmlns=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:sdg=\"http://data.europa.eu/p4s\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\">\n" +
                "  <rs:Exception xsi:type=\"rs:InvalidRequestExceptionType\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\" message=\"The query parameters do not follow the query specification\" code=\"DSD:ERR:0003\" detail=\"TAMPERED QUERY RESPONSE.\"/>\n" +
                "</query:QueryResponse>\n";
        String signature = "eyJhbGciOiJFZERTQSIsImtpZCI6Ik1Ha3dVYVJQTUUweEN6QUpCZ05WQkFZVEFrbFVNUTR3REFZRFZRUUlEQVZKZEdGc2VURU5NQXNHQTFVRUNnd0VRV2RKUkRFTk1Bc0dBMVVFQ3d3RVFXZEpSREVRTUE0R0ExVUVBd3dIWldReU5UVXhPUUlVY0RFcWc1SDd6aW1DZ0dFZHF0ZUl4YU9GQy9ZPSIsIng1dCNTMjU2Ijoic1FNVGlza1dMV2JTRW9pUEZIM1RtN0FiOERHYVBZMkE5ZjcydXEtS0YxYyIsIng1YyI6WyJNSUlCVlRDQ0FRY0NGSEF4S29PUis4NHBnb0JoSGFyWGlNV2poUXYyTUFVR0F5dGxjREJOTVFzd0NRWURWUVFHRXdKSlZERU9NQXdHQTFVRUNBd0ZTWFJoYkhreERUQUxCZ05WQkFvTUJFRm5TVVF4RFRBTEJnTlZCQXNNQkVGblNVUXhFREFPQmdOVkJBTU1CMlZrTWpVMU1Ua3dIaGNOTWpNeE1ERTJNVFF3TkRRM1doY05NalV3T1RFMU1UUXdORFEzV2pCTk1Rc3dDUVlEVlFRR0V3SkpWREVPTUF3R0ExVUVDQXdGU1hSaGJIa3hEVEFMQmdOVkJBb01CRUZuU1VReERUQUxCZ05WQkFzTUJFRm5TVVF4RURBT0JnTlZCQU1NQjJWa01qVTFNVGt3S2pBRkJnTXJaWEFESVFCK2VlQmptUFNrLzlrQXVNMmxXaTA2OWFUQ3BtRlp6L0hXN3ljY20yUUZJekFGQmdNclpYQURRUURkcldTdjJUWHNmL3FuTTdOOWNGRHFkWm50ZjhjaWk2am4vL3dNeEcweHMvekMybUdkVDc0dzV3VDdqSk5EcGFrVm92cmRuYkUybVVTWUI1YWppRFVIIl0sInR5cCI6Impvc2UiLCJiNjQiOmZhbHNlLCJzaWdUIjoiMjAyMy0xMi0yMFQxNToyNDoxMVoiLCJzaWdEIjp7Im1JZCI6Imh0dHA6Ly91cmkuZXRzaS5vcmcvMTkxODIvSHR0cEhlYWRlcnMiLCJwYXJzIjpbImRpZ2VzdCJdfSwiY3JpdCI6WyJiNjQiLCJzaWdUIiwic2lnRCJdfQ..KbONP_Hj1fSwL1_h-UBHOj8E-ZkMbNI3C0Chup5htO-cvRTC9pKgGK40g2JbKAF68iCJe4xyFC0wnHZ_h1yLAA";
        String digest = "SHA-512=RHkXWuDWRZ25GD/qC6opnKdJdqdY07vo4NdciXg2/bm0HialSWXnlesDgRKhMVu+24d6l6h7/uHf97pZRHjvJA==";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertFalse(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testValidCsSignatureDev() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator();

        String xmlQueryResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<query:QueryResponse status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:ns7=\"http://www.w3.org/1999/xhtml\" xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:ws-addr=\"http://www.w3.org/2005/08/addressing\" xmlns:oots=\"http://data.europa.eu/p4s\" xmlns:spi=\"urn:oasis:names:tc:ebxml-regrep:xsd:spi:4.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
                "    <rs:Exception xsi:type=\"rs:InvalidRequestExceptionType\" code=\"EB:ERR:0004\" detail=\"Unknown Jurisdiction Level Code\" message=\"The jurisdiction level code query parameter is invalid or unknown\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/>\n" +
                "</query:QueryResponse>";
        String signature = "eyJhbGciOiJFUzI1NiIsImtpZCI6Ik1GWXdUcVJNTUVveERUQUxCZ05WQkFvTUJFOVBWRk14SWpBZ0JnTlZCQXNNR1VaUFVpQlVSVk5VU1U1SElGQlZVbEJQVTBWVElFOU9URmt4RlRBVEJnTlZCQU1NREc5dmRITXRZM010ZEdWemRBSUVaVnhnRXc9PSIsIng1dCNTMjU2IjoiYU5XLWlxZW1XbDZYbnlZcVRFaFFuOHBGQ3ZTeDhTSHF1VVg2Mndxb0QwVSIsIng1YyI6WyJNSUlCaFRDQ0FTcWdBd0lCQWdJRVpWeGdFekFLQmdncWhrak9QUVFEQWpCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdIaGNOTWpNeE1USXhNRGMwTlRJeldoY05NalF4TVRJd01EYzBOVEl6V2pCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdXVEFUQmdjcWhrak9QUUlCQmdncWhrak9QUU1CQndOQ0FBUmE4TTJtVnA0a0VPc2d0SVZiZ2tleXc1ZDdCeFFvOG9pdDkwalJRZXhLM2VFYWtOTFhCVUI1S2ljZ0xqZk02ejlRZGZFeStpc3RxQlNzeXZoa0RuNmZNQW9HQ0NxR1NNNDlCQU1DQTBrQU1FWUNJUUMyS2NIR3UyNUNwM1VjVmREUGhDK213MGVXZXZ4SFV6NWNZYmxKaDBnUXdBSWhBT2Z6S24zeEhCSDhRNzVSZVZPMTUxdkRvdFRaQW5adkFVb1NSaWJoeWVTVyJdLCJ0eXAiOiJqb3NlIiwiYjY0IjpmYWxzZSwic2lnVCI6IjIwMjMtMTItMjFUMTU6NTk6MzZaIiwic2lnRCI6eyJtSWQiOiJodHRwOi8vdXJpLmV0c2kub3JnLzE5MTgyL0h0dHBIZWFkZXJzIiwicGFycyI6WyJkaWdlc3QiXX0sImNyaXQiOlsiYjY0Iiwic2lnVCIsInNpZ0QiXX0..VpA870tbyxRj0QQ8xAATtsHzEIg7z7_wU-owuTpKm74HoAFivzdhySOEUwCLyqclTpjWepgbc6EGjfnpNl1_0w";
        String digest = "SHA-256=dQamrk5pM31JXLcTg1+axqO/nG60YIpO2yctoMN6mQo=";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertTrue(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testTamperedSignatureCsSignatureDev() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator();

        String xmlQueryResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<query:QueryResponse status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:ns7=\"http://www.w3.org/1999/xhtml\" xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:ws-addr=\"http://www.w3.org/2005/08/addressing\" xmlns:oots=\"http://data.europa.eu/p4s\" xmlns:spi=\"urn:oasis:names:tc:ebxml-regrep:xsd:spi:4.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
                "    <rs:Exception xsi:type=\"rs:InvalidRequestExceptionType\" code=\"EB:ERR:0004\" detail=\"Unknown Jurisdiction Level Code\" message=\"The jurisdiction level code query parameter is invalid or unknown\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/>\n" +
                "</query:QueryResponse>";
        String signature = "eyJhbGciOiJFUzI1NiIsImtpZCI6Ik1GWXdUcVJNTUVveERUQUxCZ05WQkFvTUJFOVBWRk14SWpBZ0JnTlZCQXNNR1VaUFVpQlVSVk5VU1U1SElGQlZVbEJQVTBWVElFOU9URmt4RlRBVEJnTlZCQU1NREc5dmRITXRZM010ZEdWemRBSUVaVnhnRXc9PSIsIng1dCNTMjU2IjoiYU5XLWlxZW1XbDZYbnlZcVRFaFFuOHBGQ3ZTeDhTSHF1VVg2Mndxb0QwVSIsIng1YyI6WyJNSUlCaFRDQ0FTcWdBd0lCQWdJRVpWeGdFekFLQmdncWhrak9QUVFEQWpCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdIaGNOTWpNeE1USXhNRGMwTlRJeldoY05NalF4TVRJd01EYzBOVEl6V2pCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdXVEFUQmdjcWhrak9QUUlCQmdncWhrak9QUU1CQndOQ0FBUmE4TTJtVnA0a0VPc2d0SVZiZ2tleXc1ZDdCeFFvOG9pdDkwalJRZXhLM2VFYWtOTFhCVUI1S2ljZ0xqZk02ejlRZGZFeStpc3RxQlNzeXZoa0RuNmZNQW9HQ0NxR1NNNDlCQU1DQTBrQU1FWUNJUUMyS2NIR3UyNUNwM1VjVmREUGhDK213MGVXZXZ4SFV6NWNZYmxKaDBnUXdBSWhBT2Z6S24zeEhCSDhRNzVSZVZPMTUxdkRvdFRaQW5adkFVb1NSaWJoeWVTVyJdLCJ0eXAiOiJqb3NlIiwiYjY0IjpmYWxzZSwic2lnVCI6IjIwMjMtMTItMjFUMTU6NTk6MzZaIiwic2lnRCI6eyJtSWQiOiJodHRwOi8vdXJpLmV0c2kub3JnLzE5MTgyL0h0dHBIZWFkZXJzIiwicGFycyI6WyJkaWdlc3QiXX0sImNyaXQiOlsiYjY0Iiwic2lnVCIsInNpZ0QiXX0..VpA870tbyxRj0QQ8xAATtsHzEIg7z7_wU-owuTpKm74HoAFivzdhySOEUwCLyqclTpjWepgbc6EGjfTAMPERED";
        String digest = "SHA-256=dQamrk5pM31JXLcTg1+axqO/nG60YIpO2yctoMN6mQo=";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertFalse(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testTamperedDigestCsSignatureDev() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator();

        String xmlQueryResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<query:QueryResponse status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:ns7=\"http://www.w3.org/1999/xhtml\" xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:ws-addr=\"http://www.w3.org/2005/08/addressing\" xmlns:oots=\"http://data.europa.eu/p4s\" xmlns:spi=\"urn:oasis:names:tc:ebxml-regrep:xsd:spi:4.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
                "    <rs:Exception xsi:type=\"rs:InvalidRequestExceptionType\" code=\"EB:ERR:0004\" detail=\"Unknown Jurisdiction Level Code\" message=\"The jurisdiction level code query parameter is invalid or unknown\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/>\n" +
                "</query:QueryResponse>";
        String signature = "eyJhbGciOiJFUzI1NiIsImtpZCI6Ik1GWXdUcVJNTUVveERUQUxCZ05WQkFvTUJFOVBWRk14SWpBZ0JnTlZCQXNNR1VaUFVpQlVSVk5VU1U1SElGQlZVbEJQVTBWVElFOU9URmt4RlRBVEJnTlZCQU1NREc5dmRITXRZM010ZEdWemRBSUVaVnhnRXc9PSIsIng1dCNTMjU2IjoiYU5XLWlxZW1XbDZYbnlZcVRFaFFuOHBGQ3ZTeDhTSHF1VVg2Mndxb0QwVSIsIng1YyI6WyJNSUlCaFRDQ0FTcWdBd0lCQWdJRVpWeGdFekFLQmdncWhrak9QUVFEQWpCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdIaGNOTWpNeE1USXhNRGMwTlRJeldoY05NalF4TVRJd01EYzBOVEl6V2pCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdXVEFUQmdjcWhrak9QUUlCQmdncWhrak9QUU1CQndOQ0FBUmE4TTJtVnA0a0VPc2d0SVZiZ2tleXc1ZDdCeFFvOG9pdDkwalJRZXhLM2VFYWtOTFhCVUI1S2ljZ0xqZk02ejlRZGZFeStpc3RxQlNzeXZoa0RuNmZNQW9HQ0NxR1NNNDlCQU1DQTBrQU1FWUNJUUMyS2NIR3UyNUNwM1VjVmREUGhDK213MGVXZXZ4SFV6NWNZYmxKaDBnUXdBSWhBT2Z6S24zeEhCSDhRNzVSZVZPMTUxdkRvdFRaQW5adkFVb1NSaWJoeWVTVyJdLCJ0eXAiOiJqb3NlIiwiYjY0IjpmYWxzZSwic2lnVCI6IjIwMjMtMTItMjFUMTU6NTk6MzZaIiwic2lnRCI6eyJtSWQiOiJodHRwOi8vdXJpLmV0c2kub3JnLzE5MTgyL0h0dHBIZWFkZXJzIiwicGFycyI6WyJkaWdlc3QiXX0sImNyaXQiOlsiYjY0Iiwic2lnVCIsInNpZ0QiXX0..VpA870tbyxRj0QQ8xAATtsHzEIg7z7_wU-owuTpKm74HoAFivzdhySOEUwCLyqclTpjWepgbc6EGjfnpNl1_0w";
        String digest = "SHA-256=TAMPEREDM31JXLcTg1+axqO/nG60YIpO2yctoMN6mQo=";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertFalse(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testDigestNotMatchingDigestCsSignatureDev() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator();

        String xmlQueryResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<query:QueryResponse status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:ns7=\"http://www.w3.org/1999/xhtml\" xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:ws-addr=\"http://www.w3.org/2005/08/addressing\" xmlns:oots=\"http://data.europa.eu/p4s\" xmlns:spi=\"urn:oasis:names:tc:ebxml-regrep:xsd:spi:4.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
                "    <rs:Exception xsi:type=\"rs:InvalidRequestExceptionType\" code=\"EB:ERR:0004\" detail=\"Unknown Jurisdiction Level Code\" message=\" TAMPERED The jurisdiction level code query parameter is invalid or unknown\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/>\n" +
                "</query:QueryResponse>";
        String signature = "eyJhbGciOiJFUzI1NiIsImtpZCI6Ik1GWXdUcVJNTUVveERUQUxCZ05WQkFvTUJFOVBWRk14SWpBZ0JnTlZCQXNNR1VaUFVpQlVSVk5VU1U1SElGQlZVbEJQVTBWVElFOU9URmt4RlRBVEJnTlZCQU1NREc5dmRITXRZM010ZEdWemRBSUVaVnhnRXc9PSIsIng1dCNTMjU2IjoiYU5XLWlxZW1XbDZYbnlZcVRFaFFuOHBGQ3ZTeDhTSHF1VVg2Mndxb0QwVSIsIng1YyI6WyJNSUlCaFRDQ0FTcWdBd0lCQWdJRVpWeGdFekFLQmdncWhrak9QUVFEQWpCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdIaGNOTWpNeE1USXhNRGMwTlRJeldoY05NalF4TVRJd01EYzBOVEl6V2pCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdXVEFUQmdjcWhrak9QUUlCQmdncWhrak9QUU1CQndOQ0FBUmE4TTJtVnA0a0VPc2d0SVZiZ2tleXc1ZDdCeFFvOG9pdDkwalJRZXhLM2VFYWtOTFhCVUI1S2ljZ0xqZk02ejlRZGZFeStpc3RxQlNzeXZoa0RuNmZNQW9HQ0NxR1NNNDlCQU1DQTBrQU1FWUNJUUMyS2NIR3UyNUNwM1VjVmREUGhDK213MGVXZXZ4SFV6NWNZYmxKaDBnUXdBSWhBT2Z6S24zeEhCSDhRNzVSZVZPMTUxdkRvdFRaQW5adkFVb1NSaWJoeWVTVyJdLCJ0eXAiOiJqb3NlIiwiYjY0IjpmYWxzZSwic2lnVCI6IjIwMjMtMTItMjFUMTU6NTk6MzZaIiwic2lnRCI6eyJtSWQiOiJodHRwOi8vdXJpLmV0c2kub3JnLzE5MTgyL0h0dHBIZWFkZXJzIiwicGFycyI6WyJkaWdlc3QiXX0sImNyaXQiOlsiYjY0Iiwic2lnVCIsInNpZ0QiXX0..VpA870tbyxRj0QQ8xAATtsHzEIg7z7_wU-owuTpKm74HoAFivzdhySOEUwCLyqclTpjWepgbc6EGjfnpNl1_0w";
        String digest = "SHA-256=dQamrk5pM31JXLcTg1+axqO/nG60YIpO2yctoMN6mQo=";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertFalse(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testValidCsSignatureAcc() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator();

        String xmlQueryResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<query:QueryResponse xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:oots=\"http://data.europa.eu/p4s\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:spi=\"urn:oasis:names:tc:ebxml-regrep:xsd:spi:4.0\" xmlns:ns7=\"http://www.w3.org/1999/xhtml\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:ws-addr=\"http://www.w3.org/2005/08/addressing\" status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\">\n" +
                "    <rs:Exception xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"rs:InvalidRequestExceptionType\" code=\"EB:ERR:0005\" detail=\"UNKNOWN_PROCEDURE\" message=\"The value of the procedure-id query parameter is invalid or unknown\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\"/>\n" +
                "</query:QueryResponse>\n";
        String signature = "eyJhbGciOiJFUzI1NiIsImtpZCI6Ik1FY3dRS1ErTUR3eEhEQWFCZ05WQkFvTUUwVjFjbTl3WldGdUlFTnZiVzFwYzNOcGIyNHhIREFhQmdOVkJBTU1FME52YlcxcGMxTnBaMjRnTFNBeUlIUmxjM1FDQXdFUW9RPT0iLCJ4NXQjUzI1NiI6Im80NkZxc2ZTLVBONjBlaUszY3dqcXozc3lNdG91ZFplbUpPSlk1UHZwcHciLCJ4NWMiOlsiTUlJRTRUQ0NBc21nQXdJQkFnSURBUkNoTUEwR0NTcUdTSWIzRFFFQkN3VUFNRHd4SERBYUJnTlZCQW9NRTBWMWNtOXdaV0Z1SUVOdmJXMXBjM05wYjI0eEhEQWFCZ05WQkFNTUUwTnZiVzFwYzFOcFoyNGdMU0F5SUhSbGMzUXdIaGNOTWpNeE1USXhNVEkxTURNNFdoY05NalV4TVRJeE1USTFNRE00V2pCQU1Rc3dDUVlEVlFRR0V3SkNSVEVjTUJvR0ExVUVDZ3dUUlhWeWIzQmxZVzRnUTI5dGJXbHpjMmx2YmpFVE1CRUdBMVVFQXd3S1JVTXRUMDlVVXkxRFV6QlpNQk1HQnlxR1NNNDlBZ0VHQ0NxR1NNNDlBd0VIQTBJQUJDaElQNlB6SEZJTi9oN1E4UWZMcEx5TTdHbkZUbXdEc1BJZ1VpUW1JdElZUmFNQmRvSnhsRFUwMFJEV2g1TlVjZVliZ1BDbmN4ekhDc1VrdmhIMm0xQ2pnZ0d4TUlJQnJUQWRCZ05WSFE0RUZnUVVHMU1FT2NrQlFBMTQzczBienQ3TFA0S2lHem93SHdZRFZSMGpCQmd3Rm9BVWJCMmQzRjhSU3Rac2c5Y2oraXMwd2pXSzd5WXdlQVlEVlIwZ0JIRXdiekJ0QmdjcmdRSUNBUWtDTUdJd1B3WUlLd1lCQlFVSEFnRVdNMmgwZEhCek9pOHZZMjl0YldsemMybG5iaTV3YTJrdVpXTXVaWFZ5YjNCaExtVjFMMmx1Wm04dlkzQXZRMUJUTG5Ca1pqQWZCZ2dyQmdFRkJRY0NBakFUR2hGVVpYTjBJSEIxY25CdmMyVWdiMjVzZVRBT0JnTlZIUThCQWY4RUJBTUNCc0F3VGdZRFZSMGZCRWN3UlRCRG9FR2dQNFk5YUhSMGNEb3ZMMk52YlcxcGMzTnBaMjR1Y0d0cExtVmpMbVYxY205d1lTNWxkUzlwYm1adkwyTnliQzl2Ym14cGJtVkRRVjkwWlhOMExtTnliRENCa0FZSUt3WUJCUVVIQVFFRWdZTXdnWUF3U2dZSUt3WUJCUVVITUFLR1BtaDBkSEJ6T2k4dlkyOXRiV2x6YzJsbmJpNXdhMmt1WldNdVpYVnliM0JoTG1WMUwybHVabTh2WVdsaEwyOXViR2x1WlVOQlgzUmxjM1F1WTNKME1ESUdDQ3NHQVFVRkJ6QUJoaVpvZEhSd09pOHZiMk56Y0MxMFpYTjBMbU52YlcxcGMzTnBaMjR1WTJWakxtVjFMbWx1ZERBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQWdFQW5XZzhMN29GWkFKb3BYR2FTT1ZENmhtU0VQSFBobElVMXprcjhielFENW5RUDZSME1RY0FScmJzYVJub0dER293MURNZE9kN3EzTVpHdUxDWWsxV2VBWjRocnZTSHZXZERDN0ljOU1LZHdRZi8yNEdlMTg4TDZ1NktGdHlvYzZ5ZHJLSW4zbDh6bDBVMDliN1kyeGpubk9aSCtJZUJNbHQ1SHJtVU5QWXc1RFI0WE5YL29Ea2k1WFNrc2xkNnQ0cVVSWGhmcFBFSWE4VkhxMXJVOGNwU2paSjBseDFkQlVvQUIwN1BoalJ4S0cra1V0emxzcm5neHRlMWg2NDRSYXZIRXRGS1hGdEZxUHc5R2ZwVzNPdEdIRUdJcDliYkFtQk1YSUlJUjJsQkh0OGJvbzZ0TUJ1U1JTVHMxWDYzUmdjTEFsZ1B2SnJLTThrZWFmTGVITGNiWExJM2VJTlE0TlE4QTV3QjFBTFRPQjZ6L3dleWw0dUtTZDRPZVVkMFo5ZjZybVpTeC9ka295cjF5cjg1TUVSZk51WmlRSFJ4QWZQNDdIRDRxb1AvS3lLWHZNMCtORnhlRVcremt6RlVwSVhnZy9id3kwMjFpTmdJRnQ5ZitacjNIRHV3dE8zRUxQUHF1MHNXRittQzhRS01nU2tWZmNGNlRjSVVoRE4vNVVBTmtnK0NGTC9sWHplTC9oTHZicDJJREh4bENvQ09vTk1QemNhdldlSFZlTlFkMFB5Z09TU0tWY0VhRGw5VTFEU3h2MmZhOEJRNkFsMDRVWVhLUThueDhpSWRmU3l6TXdNU1d0NTN2dXkwZzNtakpydW8rWVZsMDBBdHNHa3Nhb0p3TUFHUDVvdGpHcWJXVWxIZWt4RjRJSTQvWUx2ZVpnOU5zbG1pVHN6REZJPSJdLCJ0eXAiOiJqb3NlIiwiYjY0IjpmYWxzZSwic2lnVCI6IjIwMjMtMTItMjFUMTY6MjU6NDlaIiwic2lnRCI6eyJtSWQiOiJodHRwOi8vdXJpLmV0c2kub3JnLzE5MTgyL0h0dHBIZWFkZXJzIiwicGFycyI6WyJkaWdlc3QiXX0sImNyaXQiOlsiYjY0Iiwic2lnVCIsInNpZ0QiXX0..dAMWXJDDIMVaGXE9c8EhBtYut2lMF7BN1Gh_h-kH3ZKJmvpbZoWmzmGSzq107LKdR25x0uFSLbCO2IGGGi0gtw";
        String digest = "SHA-256=oa3/JPaWhj4CUy9h3/7O2M0Tmcx8ZGWBGZv5M+JfAC0=";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertTrue(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testValidCsSignatureProd() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator();

        String xmlQueryResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<query:QueryResponse xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:oots=\"http://data.europa.eu/p4s\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:spi=\"urn:oasis:names:tc:ebxml-regrep:xsd:spi:4.0\" xmlns:ns7=\"http://www.w3.org/1999/xhtml\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:ws-addr=\"http://www.w3.org/2005/08/addressing\" status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\">\n" +
                "    <rs:Exception xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"rs:InvalidRequestExceptionType\" code=\"EB:ERR:0006\" detail=\"UNKNOWN_PROCEDURE_COUNTRY\" message=\"The value of the procedure implementation country query parameter is invalid or unknown\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\"/>\n" +
                "</query:QueryResponse>\n";
        String signature = "eyJhbGciOiJFUzI1NiIsImtpZCI6Ik1FSXdPNlE1TURjeEhEQWFCZ05WQkFvTUUwVjFjbTl3WldGdUlFTnZiVzFwYzNOcGIyNHhGekFWQmdOVkJBTU1Ea052YlcxcGMxTnBaMjRnTFNBeUFnTUg2NDQ9IiwieDV0I1MyNTYiOiJ3dkpydWY5eDQ4YWxCSi1rNVdGRjBNTWpzN0xxOTRPWWI3em1YWmUtSUVRIiwieDVjIjpbIk1JSUVWakNDQWo2Z0F3SUJBZ0lEQit1T01BMEdDU3FHU0liM0RRRUJDd1VBTURjeEhEQWFCZ05WQkFvTUUwVjFjbTl3WldGdUlFTnZiVzFwYzNOcGIyNHhGekFWQmdOVkJBTU1Ea052YlcxcGMxTnBaMjRnTFNBeU1CNFhEVEl6TVRFeU9UQTVORGN6TVZvWERUSTFNVEV5T1RBNU5EY3pNVm93UURFTE1Ba0dBMVVFQmhNQ1FrVXhIREFhQmdOVkJBb01FMFYxY205d1pXRnVJRU52YlcxcGMzTnBiMjR4RXpBUkJnTlZCQU1NQ2tWRExVOVBWRk10UTFNd1dUQVRCZ2NxaGtqT1BRSUJCZ2dxaGtqT1BRTUJCd05DQUFSblFIVlVuRlBQZDJOYlJtUU5UY3lEUFNPVnBuTy96dTdua3B0Y1gvditYM01NNjhYZldsUWhuZGVxc242UFpxUCtHS2QzdmtHL1g1eCs3bXVyWnhGY280SUJLekNDQVNjd0hRWURWUjBPQkJZRUZBS251NmRiVXFHeGhVQllBZmlIVUhGK2Y1TTBNQjhHQTFVZEl3UVlNQmFBRkpyN2ozWm1tTnlzTE5kM05uRnR1dU5uUjVIMk1BNEdBMVVkRHdFQi93UUVBd0lHd0RCSkJnTlZIUjhFUWpCQU1ENmdQS0E2aGpob2RIUndPaTh2WTI5dGJXbHpjMmxuYmk1d2Eya3VaV011WlhWeWIzQmhMbVYxTDJsdVptOHZZM0pzTDI5dWJHbHVaVU5CTG1OeWJEQ0JpUVlJS3dZQkJRVUhBUUVFZlRCN01FUUdDQ3NHQVFVRkJ6QUNoamhvZEhSd09pOHZZMjl0YldsemMybG5iaTV3YTJrdVpXTXVaWFZ5YjNCaExtVjFMMmx1Wm04dllXbGhMMjl1YkdsdVpVTkJMbU55ZERBekJnZ3JCZ0VGQlFjd0FZWW5hSFIwY0RvdkwyTnZiVzFwYzNOcFoyNHVjR3RwTG1WakxtVjFjbTl3WVM1bGRTOXZZM053TUEwR0NTcUdTSWIzRFFFQkN3VUFBNElDQVFCZEJwaE1rL1BlUUtIOWxidzJrRTBUbytGNE85ZG9vUmxpcnEvSzRzR3ZONnJCN1NSL3RiZ3dWZTE3alFIbERndmNGZTRDVlVrS09nY2NhV29FVHFGTC9BU1RNSmdobTlacGl0NHhQbHBuajZDc2FISUFHL283RXk5aEZGWUdMeExXTmhPUzA1K1JmeVpWcDNvSjNLcGFWY0s3ZDVHei82bk8wQ2wwNFdJZ1lGeURUWU13YU4raytoSFFBcXB6SDdEeFlxMDVhTFdZbDdYUklrY1dOS05YS2ttV1ZlREUwR1c0aHNPQnFQUEs3aEIvbTcxbDhNZkVjTVlVZzFNaFo3L2NHUWE2UCsxNmd1RlFtZm03L0JIcTRiOHNvTmNLZjc4RDNJc0RyTFVST3duMEhPSU45T3BiQzdnMEtNOFZMZ0FXV0xIRVZFaS9YVGN1aUxPSldoUlNkRHFRenA2Y2kvUGhYLzJGNVJMOWlHd2p4ank0WlF2d2R0bk1BdFhraFFCNUtNekRYM0F4emhLNEhMVDNSWnhuUWNVRnJaeDJsNXpRaDU2d3k2dkJCWVdBNE9UU2VUVGlBYkVJejlHRnJoYVkrbDhTTGNhc1llbWdEY2pGaFk3YTA5K29vSjVDSFZyd1ZTUkh1OVFsdTN0ZVNwUVFyNDZSaE9tb3ZlVk1JczZYL3lIRnJ4a2J3dk9KenNqbEtwNDFwVEpHMTlEMTkvb0FSczlwSWZWTk5UVVJ4YXl2UlZZN0NRQlQvMWlsODV0S01WNTFNZ3ZBMENVMnJQQlByTnE0K0hEcjZvc0dyclFRbzJJdXdSR0lSa2czaDRsMXNaZHRJUnhYT1pvUndTbDB0YWhQTys5RGFhb2NsZmFDZHNscVh5R1VHbXFxZGFCVEhGR2xmKy9nWUE9PSJdLCJ0eXAiOiJqb3NlIiwiYjY0IjpmYWxzZSwic2lnVCI6IjIwMjMtMTItMjFUMTY6MzM6MTlaIiwic2lnRCI6eyJtSWQiOiJodHRwOi8vdXJpLmV0c2kub3JnLzE5MTgyL0h0dHBIZWFkZXJzIiwicGFycyI6WyJkaWdlc3QiXX0sImNyaXQiOlsiYjY0Iiwic2lnVCIsInNpZ0QiXX0..VfzAdci0NZjCb7gmifAq_lQN-cvDdlTJl1Ij6eUlm5W621TCID6qFryip095_5tnjCTW37x5hiK68DEVr5SQlA";
        String digest = "SHA-256=ASao0hM580kRafkXphlLuIAvWLPVsCBrjd7FoJCxa6s=";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertTrue(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testValidJustProdTruststoreCsSignatureProd() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator("cs-test-just-prod-truststore.jks", "oots123");

        String xmlQueryResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<query:QueryResponse xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:oots=\"http://data.europa.eu/p4s\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:spi=\"urn:oasis:names:tc:ebxml-regrep:xsd:spi:4.0\" xmlns:ns7=\"http://www.w3.org/1999/xhtml\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:ws-addr=\"http://www.w3.org/2005/08/addressing\" status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\">\n" +
                "    <rs:Exception xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"rs:InvalidRequestExceptionType\" code=\"EB:ERR:0006\" detail=\"UNKNOWN_PROCEDURE_COUNTRY\" message=\"The value of the procedure implementation country query parameter is invalid or unknown\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\"/>\n" +
                "</query:QueryResponse>\n";
        String signature = "eyJhbGciOiJFUzI1NiIsImtpZCI6Ik1FSXdPNlE1TURjeEhEQWFCZ05WQkFvTUUwVjFjbTl3WldGdUlFTnZiVzFwYzNOcGIyNHhGekFWQmdOVkJBTU1Ea052YlcxcGMxTnBaMjRnTFNBeUFnTUg2NDQ9IiwieDV0I1MyNTYiOiJ3dkpydWY5eDQ4YWxCSi1rNVdGRjBNTWpzN0xxOTRPWWI3em1YWmUtSUVRIiwieDVjIjpbIk1JSUVWakNDQWo2Z0F3SUJBZ0lEQit1T01BMEdDU3FHU0liM0RRRUJDd1VBTURjeEhEQWFCZ05WQkFvTUUwVjFjbTl3WldGdUlFTnZiVzFwYzNOcGIyNHhGekFWQmdOVkJBTU1Ea052YlcxcGMxTnBaMjRnTFNBeU1CNFhEVEl6TVRFeU9UQTVORGN6TVZvWERUSTFNVEV5T1RBNU5EY3pNVm93UURFTE1Ba0dBMVVFQmhNQ1FrVXhIREFhQmdOVkJBb01FMFYxY205d1pXRnVJRU52YlcxcGMzTnBiMjR4RXpBUkJnTlZCQU1NQ2tWRExVOVBWRk10UTFNd1dUQVRCZ2NxaGtqT1BRSUJCZ2dxaGtqT1BRTUJCd05DQUFSblFIVlVuRlBQZDJOYlJtUU5UY3lEUFNPVnBuTy96dTdua3B0Y1gvditYM01NNjhYZldsUWhuZGVxc242UFpxUCtHS2QzdmtHL1g1eCs3bXVyWnhGY280SUJLekNDQVNjd0hRWURWUjBPQkJZRUZBS251NmRiVXFHeGhVQllBZmlIVUhGK2Y1TTBNQjhHQTFVZEl3UVlNQmFBRkpyN2ozWm1tTnlzTE5kM05uRnR1dU5uUjVIMk1BNEdBMVVkRHdFQi93UUVBd0lHd0RCSkJnTlZIUjhFUWpCQU1ENmdQS0E2aGpob2RIUndPaTh2WTI5dGJXbHpjMmxuYmk1d2Eya3VaV011WlhWeWIzQmhMbVYxTDJsdVptOHZZM0pzTDI5dWJHbHVaVU5CTG1OeWJEQ0JpUVlJS3dZQkJRVUhBUUVFZlRCN01FUUdDQ3NHQVFVRkJ6QUNoamhvZEhSd09pOHZZMjl0YldsemMybG5iaTV3YTJrdVpXTXVaWFZ5YjNCaExtVjFMMmx1Wm04dllXbGhMMjl1YkdsdVpVTkJMbU55ZERBekJnZ3JCZ0VGQlFjd0FZWW5hSFIwY0RvdkwyTnZiVzFwYzNOcFoyNHVjR3RwTG1WakxtVjFjbTl3WVM1bGRTOXZZM053TUEwR0NTcUdTSWIzRFFFQkN3VUFBNElDQVFCZEJwaE1rL1BlUUtIOWxidzJrRTBUbytGNE85ZG9vUmxpcnEvSzRzR3ZONnJCN1NSL3RiZ3dWZTE3alFIbERndmNGZTRDVlVrS09nY2NhV29FVHFGTC9BU1RNSmdobTlacGl0NHhQbHBuajZDc2FISUFHL283RXk5aEZGWUdMeExXTmhPUzA1K1JmeVpWcDNvSjNLcGFWY0s3ZDVHei82bk8wQ2wwNFdJZ1lGeURUWU13YU4raytoSFFBcXB6SDdEeFlxMDVhTFdZbDdYUklrY1dOS05YS2ttV1ZlREUwR1c0aHNPQnFQUEs3aEIvbTcxbDhNZkVjTVlVZzFNaFo3L2NHUWE2UCsxNmd1RlFtZm03L0JIcTRiOHNvTmNLZjc4RDNJc0RyTFVST3duMEhPSU45T3BiQzdnMEtNOFZMZ0FXV0xIRVZFaS9YVGN1aUxPSldoUlNkRHFRenA2Y2kvUGhYLzJGNVJMOWlHd2p4ank0WlF2d2R0bk1BdFhraFFCNUtNekRYM0F4emhLNEhMVDNSWnhuUWNVRnJaeDJsNXpRaDU2d3k2dkJCWVdBNE9UU2VUVGlBYkVJejlHRnJoYVkrbDhTTGNhc1llbWdEY2pGaFk3YTA5K29vSjVDSFZyd1ZTUkh1OVFsdTN0ZVNwUVFyNDZSaE9tb3ZlVk1JczZYL3lIRnJ4a2J3dk9KenNqbEtwNDFwVEpHMTlEMTkvb0FSczlwSWZWTk5UVVJ4YXl2UlZZN0NRQlQvMWlsODV0S01WNTFNZ3ZBMENVMnJQQlByTnE0K0hEcjZvc0dyclFRbzJJdXdSR0lSa2czaDRsMXNaZHRJUnhYT1pvUndTbDB0YWhQTys5RGFhb2NsZmFDZHNscVh5R1VHbXFxZGFCVEhGR2xmKy9nWUE9PSJdLCJ0eXAiOiJqb3NlIiwiYjY0IjpmYWxzZSwic2lnVCI6IjIwMjMtMTItMjFUMTY6MzM6MTlaIiwic2lnRCI6eyJtSWQiOiJodHRwOi8vdXJpLmV0c2kub3JnLzE5MTgyL0h0dHBIZWFkZXJzIiwicGFycyI6WyJkaWdlc3QiXX0sImNyaXQiOlsiYjY0Iiwic2lnVCIsInNpZ0QiXX0..VfzAdci0NZjCb7gmifAq_lQN-cvDdlTJl1Ij6eUlm5W621TCID6qFryip095_5tnjCTW37x5hiK68DEVr5SQlA";
        String digest = "SHA-256=ASao0hM580kRafkXphlLuIAvWLPVsCBrjd7FoJCxa6s=";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertTrue(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }

    @Test
    void testInvalidJustProdTruststoreCsSignatureDev() {
        CsSignatureValidator csSignatureValidator = new CsSignatureValidator("cs-test-just-prod-truststore.jks", "oots123");

        String xmlQueryResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<query:QueryResponse status=\"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0\" xmlns:ns7=\"http://www.w3.org/1999/xhtml\" xmlns:lcm=\"urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0\" xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0\" xmlns:ws-addr=\"http://www.w3.org/2005/08/addressing\" xmlns:oots=\"http://data.europa.eu/p4s\" xmlns:spi=\"urn:oasis:names:tc:ebxml-regrep:xsd:spi:4.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
                "    <rs:Exception xsi:type=\"rs:InvalidRequestExceptionType\" code=\"EB:ERR:0004\" detail=\"Unknown Jurisdiction Level Code\" message=\"The jurisdiction level code query parameter is invalid or unknown\" severity=\"urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/>\n" +
                "</query:QueryResponse>";
        String signature = "eyJhbGciOiJFUzI1NiIsImtpZCI6Ik1GWXdUcVJNTUVveERUQUxCZ05WQkFvTUJFOVBWRk14SWpBZ0JnTlZCQXNNR1VaUFVpQlVSVk5VU1U1SElGQlZVbEJQVTBWVElFOU9URmt4RlRBVEJnTlZCQU1NREc5dmRITXRZM010ZEdWemRBSUVaVnhnRXc9PSIsIng1dCNTMjU2IjoiYU5XLWlxZW1XbDZYbnlZcVRFaFFuOHBGQ3ZTeDhTSHF1VVg2Mndxb0QwVSIsIng1YyI6WyJNSUlCaFRDQ0FTcWdBd0lCQWdJRVpWeGdFekFLQmdncWhrak9QUVFEQWpCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdIaGNOTWpNeE1USXhNRGMwTlRJeldoY05NalF4TVRJd01EYzBOVEl6V2pCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdXVEFUQmdjcWhrak9QUUlCQmdncWhrak9QUU1CQndOQ0FBUmE4TTJtVnA0a0VPc2d0SVZiZ2tleXc1ZDdCeFFvOG9pdDkwalJRZXhLM2VFYWtOTFhCVUI1S2ljZ0xqZk02ejlRZGZFeStpc3RxQlNzeXZoa0RuNmZNQW9HQ0NxR1NNNDlCQU1DQTBrQU1FWUNJUUMyS2NIR3UyNUNwM1VjVmREUGhDK213MGVXZXZ4SFV6NWNZYmxKaDBnUXdBSWhBT2Z6S24zeEhCSDhRNzVSZVZPMTUxdkRvdFRaQW5adkFVb1NSaWJoeWVTVyJdLCJ0eXAiOiJqb3NlIiwiYjY0IjpmYWxzZSwic2lnVCI6IjIwMjMtMTItMjFUMTU6NTk6MzZaIiwic2lnRCI6eyJtSWQiOiJodHRwOi8vdXJpLmV0c2kub3JnLzE5MTgyL0h0dHBIZWFkZXJzIiwicGFycyI6WyJkaWdlc3QiXX0sImNyaXQiOlsiYjY0Iiwic2lnVCIsInNpZ0QiXX0..VpA870tbyxRj0QQ8xAATtsHzEIg7z7_wU-owuTpKm74HoAFivzdhySOEUwCLyqclTpjWepgbc6EGjfnpNl1_0w";
        String digest = "SHA-256=dQamrk5pM31JXLcTg1+axqO/nG60YIpO2yctoMN6mQo=";

        CsSignatureValidation csSignatureValidation = csSignatureValidator.validate(xmlQueryResponse, signature, digest);
        assertFalse(csSignatureValidation.isValid());
        assertNotNull(csSignatureValidation.getValidationInfo());
    }
}